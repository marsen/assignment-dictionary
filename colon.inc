%macro colon 2							;%1 -- dict value, %2 -- dictionary key
		%ifstr %1						;chek if value is a string
			%ifid %2					;check if key is okey


				%2:						;node label

										;link to next element
					%ifdef start		;if start is not defined -- this elem is 1st
						dq start		;set this element at start point
					%else				;if elem is 1st
						dq 0			;set it to beginig
					%endif

										;the key of node
					db %1, 0			;we define new pair key + value


					%define start %2	;make "start" point to this element
										;(update the dict start)
			%else
				%error "2nd arg has to be a dict value, so it has to be a string"
			%endif
		%else
			%error "1st arg has to be a dict key, so fit the id form"
		%endif
%endmacro
