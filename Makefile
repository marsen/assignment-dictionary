ASM=nasm
LD=ld
ASMTEGS=-felf64 -g

all: main

main.o:	main.asm lib.inc colon.inc words.inc
	$(ASM) $(ASMTEGS) -o $@ $<

main: main.o lib.o dict.o
	$(LD) -o $@ $< lib.o dict.o
	
lib.o: lib.asm
	$(ASM) $(ASMTEGS) -o $@ $<

dict.o: dict.asm lib.inc
	$(ASM) $(ASMTEGS) -o $@ $<

.PHONY: clean all

clean:
	rm -rf lib.o dict.o main.o main

