%include "colon.inc"
%include "words.inc"
%include "lib.inc"
extern find_word
global _start

section .data
	too_long_exception: db "No word or too long word, it has to be less than 256", 0
	no_such_key_exception: db "No such key in dictionary!", 0
	buffer: times 256 db 0

section .text
_start:
	mov rdi, buffer				;put start buffer to rdi to read word from it
	mov rsi, 255				;put it len there too
	call read_expression		;read key from buffer
	cmp rax, 0					;if we failed to read 
	je .wrong_word				;return with exception
								;else try to find the word in dict
	mov rdi, buffer				;put buffer pointer to args
	mov rsi, start				;put it dict start to args
	call find_word				;try to find word
	test rax, rax				;if rax is 0 -- we fail to find the word
	je .not_found

	mov rdi, rax				;move the begining of value to rdi
	call print_string			;to print value
	call print_newline			;print each word from new line
	xor rdi, rdi				;set return code 0
	call exit

.wrong_word:
	mov rdi, too_long_exception
	jmp .end

.not_found:
	mov rdi, no_such_key_exception
.end:
	call print_error
	call print_newline
	call exit
