global find_word
%include "lib.inc"
section .text

;IN		rdi: pointer to key with 0 at the end
;		rsi: the begining of dict
;OUT	rax: begining of pair key value, or 0 if fail
find_word: 
.search:
	push rsi			;save registers before calling
	push rdi
	add rsi, 8			;move for the key of this pair of key value
	call string_equals	;check if this key is the save with word in rdi
	test rax, rax		;if in rax 0 -- this isn't the rigth key
	jnz .end
	pop rdi				;restore registers
	pop rsi
	mov rsi, [rsi]		;move to next element
	test rsi, rsi		;if it isn't the end of the dict
	jne .search			;keep searching
	xor rax, rax		;we failed set 0 to rax
	ret
 .end:
	pop rdi
	call string_length	;count key length without 0-terminator
	pop rsi
	add rax, rsi		;add length of key to rax
	add rax, 9			;add len of mark and 0-terminator
	ret
